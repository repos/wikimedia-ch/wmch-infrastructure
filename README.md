# Wikimedia CH infrastructure

This repository is shared alongside some Wikimedia CH servers.

The purpose is:

* sharing resources
* tracking changes
* blame fresh regressions
* etc.

## Contact

* https://phabricator.wikimedia.org/tag/wmch-infrastructure/

## More info

* https://members.wikimedia.ch/wiki/Infrastructure

## License

This repository only contains configurations that cannot be considered creative enough to be covered by copyright.

In doubt, as default this is the explicit license:

Copyrigt (C) 2020-2023 Valerio Bozzolan, contributors

https://creativecommons.org/publicdomain/zero/1.0/
