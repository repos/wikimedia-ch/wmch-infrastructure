<VirtualHost *:443>

	ServerName fr.dicoado.org

	#Include /etc/apache2/my-include/dicoado/dicoado-mediawiki.conf
# IMPORTANT:
# This file may be read also by the demo. It will probably override just the Document Root.

DocumentRoot /home/www-dicoado/storage/fr/assets

RewriteEngine on
RewriteCond %{HTTP_HOST} ^fr\.dicoado\.org$
RewriteCond %{REQUEST_URI} ^/$
RewriteRule ^ https://fr.dicoado.org/dico/ [R=308,END]

# Let's Encrypt is configured to have webroot renewal in /var/www/html
# for all hosts, so it's super simple to renew something.
#
#  certbot certonly --webroot --webroot-path=/var/www/html -d WHATEVERDOMAIN
#
Alias /.well-known/acme-challenge /var/www/html/.well-known/acme-challenge

Alias /w/extensions/ /opt/mediawiki/1.39/extensions/
Alias /w/skins/ /opt/mediawiki/1.39/skins/
Alias /w/vendor/ /opt/mediawiki/1.39/vendor/
Alias /w/resources/ /opt/mediawiki/1.39/resources/

Alias /w/images/ /home/www-dicoado/storage/fr/images/

Alias /w/ /opt/mediawiki/farm/www/
Alias /dico  /opt/mediawiki/farm/www/index.php

#Alias /wiki  /var/www/dicoado/fr/httpdocs
#Alias /dico  /var/www/dicoado/fr/httpdocs/index.php
# At the moment these ^ are managed via .htaccess

#
# Enable .htacess files
#
# TODO: we may ignore any .htaccess and eventually include them statically there
# for performance reasons.
<Directory /opt/mediawiki>
	AllowOverride All
</Directory>

#
# Permission fixes
#
# Allow to visit alien locations, normally forbidden by apache defaults.
#<Directory /home/www-dicoado/web-resources>
<Directory /opt/mediawiki>
	Require all granted
</Directory>
<Directory /home/www-dicoado/storage/fr/assets>
	Require all granted
</Directory>
<Directory /home/www-dicoado/storage/fr/images>
	Require all granted
</Directory>

#
# Hardening
#
# Don't run arbitrary PHP code in static directories.
<Directory /home/www-dicoado/storage>
	php_admin_flag engine off
</Directory>

CustomLog ${APACHE_LOG_DIR}/dicoado-fr.log common

	# Rififi 2023.09.10 trying to fix erroneous certificates
	#   certbot certonly --apache
	# Valerio 2024.02.12 I've separated the "with and without www" certificates.
	#  also because the 'www' domain for some reason disappeared.
	#  In any case, this should work:
	#   certbot certonly --webroot --webroot-path /var/www/html -d fr.dicoado.org
	<IfFile /etc/letsencrypt/live/fr.dicoado.org-0001/fullchain.pem>
		SSLCertificateFile    /etc/letsencrypt/live/fr.dicoado.org-0001/fullchain.pem
		SSLCertificateKeyFile /etc/letsencrypt/live/fr.dicoado.org-0001/privkey.pem
		Include               /etc/apache2/my-include/ssl-hardening.conf
	</IfFile>

</VirtualHost>

<VirtualHost *:443>

	# People loves to write weird things...
	ServerName www.fr.dicoado.org

	Redirect permanent / https://fr.dicoado.org/

	# same certificate of the above that includes also the www
	<IfFile /etc/letsencrypt/live/fr.dicoado.org/fullchain.pem>
		SSLCertificateFile    /etc/letsencrypt/live/fr.dicoado.org/fullchain.pem
		SSLCertificateKeyFile /etc/letsencrypt/live/fr.dicoado.org/privkey.pem
		Include               /etc/apache2/my-include/ssl-hardening.conf
	</IfFile>

</VirtualHost>
