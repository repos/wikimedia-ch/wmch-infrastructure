Installation of MediaWikiFarm (once)
====================================

Install MediaWikiFarm code:
```
cd /opt/mediawiki
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/MediaWikiFarm.git farm
```

Here it is not used as a MediaWiki extension given it will manage multiple MediaWiki versions: it is a MediaWiki loader, so it is installed in a MediaWiki-independant directory `/opt/mediawiki/farm`. Each MediaWiki version will be installed alongside as `/opt/mediawiki/VERSION` (e.g. `/opt/mediawiki/1.39` where "1.39" is some arbitrary label).

Configure /opt/mediawiki/farm/config/MediaWikiFarmDirectories.php:
* copy it from the template config file /opt/mediawiki/farm/docs/config/MediaWikiFarmDirectories.php
* `$wgMediaWikiFarmConfigDir = '/etc/mediawiki';` (default value)
* `$wgMediaWikiFarmCodeDir = dirname( dirname( __DIR__ ) );` (default value, it means here `/opt/mediawiki`)
* `$wgMediaWikiFarmCacheDir = '/var/cache/mediawikifarm';`

Create the directories:
```
cd /home/www-dicoado/storage
sudo mkdir /var/cache/mediawikifarm
sudo chgrp www-data /var/cache/mediawikifarm
sudo chmod g+w /var/cache/mediawikifarm
sudo mkdir /etc/mediawiki
```


Install a new MediaWiki version
===============================

Install the MediaWiki version in `/opt/mediawiki/VERSION` where VERSION is an arbitrary label (advice to use the name of the version like "1.39").

You have to install MediaWiki + extensions + skins + Composer extensions/skins.

It might be either with .tar.gz for MediaWiki + extensions/skins from Gerrit, and/or with Git from Gerrit/GitHub/elsewhere.

Extensions/skins installed with Composer have a specific procedure: instead of executing `composer update --no-dev` you have to:
* execute `php /opt/mediawiki/farm/bin/mwcomposer update --no-dev`
* `cd /opt/mediawiki/1.39/vendor; for c in composer*; do cp -a composer/platform_check.php $c; done` (issue in MediaWikiFarm to be fixed)
This specific procedure for extensions/skins installed with Composer permits to selectively disable some of these extensions with the config parameters $wgUseExtensionNAME/$wgUseSkinNAME (else these extensions/skins will be actived for all wikis of the farm)

When preparing the deployment of a new MediaWiki version, you can then set the new MW label to some wiki in /etc/mediawiki/versions-prod.php. It can be done on a test wiki to fix issues before the real deployment.


Install one new language
========================

Example here for the language "en".

Create the data subdirectories:
```
cd /home/www-dicoado/storage
sudo mkdir en
sudo mkdir en/{images,cache,tmp,assets,config}
sudo chgrp www-data en/{images,cache,tmp,assets,config}
sudo chmod g+w en/{images,cache,tmp,assets,config}
```

Alternativerly it can be done in batch for multiple languages:
```
cd /home/www-dicoado/storage
sudo mkdir {fr,de,it,rm,en}
sudo mkdir {fr,de,it,rm,en}/{images,cache,tmp,assets,config}
sudo chgrp www-data {fr,de,it,rm,en}/{images,cache,tmp,assets,config}
sudo chmod g+w {fr,de,it,rm,en}/{images,cache,tmp,assets,config}
```

Purpose of each data sudirectory:
* images: [backup needed] classical subdirectory "images" where MediaWiki stores the files of the wiki (namespace File:)
* config: [backup needed] used to store the Semantic MediaWiki’s config file set by parameter $smwgConfigFileDir
* assets: [backup needed] various files served by the web server (favicons, robots.txt, logos, sitemaps, fonts, etc.)
* cache: [no backup] classical subdirectory "cache" where MediaWiki stores the compiled language files (generally files like `l10n_cache-fr.cdb`, it is set by the parameter $wgCacheDirectory)
* tmp: [no backup] set by the parameter $wgTmpDirectory; it might be `/tmp` but to avoid collisions between wikis it is preferable to set it to independent directories in a farm

Set the DNS for "en.dicoado.org" pointing to the server.

Configure the Apache files:
```
sudo cp -i /etc/apache2/sites-available/org-dicoado-rm-txt.conf /etc/apache2/sites-available/org-dicoado-en-txt.conf
sudo sed -i 's/rm\./en\./g' /etc/apache2/sites-available/org-dicoado-en-txt.conf # Change "rm" to "en" everywhere
sudo ln -s ../sites-available/org-dicoado-en-txt.conf /etc/apache2/sites-enabled/org-dicoado-en-txt.conf # Create a link to this file
sudo systemctl reload apache2.service
sudo certbot certonly --webroot -w /var/www/html -d en.dicoado.org -d www.en.dicoado.org # Obtain SSL certificates from Let’s Encrypt
sudo cp -i /etc/apache2/sites-available/org-dicoado-rm-ssl.conf /etc/apache2/sites-available/org-dicoado-en-ssl.conf
sudo sed -i 's/rm\./en\./g' /etc/apache2/sites-available/org-dicoado-en-ssl.conf # Change "rm" to "en" everywhere
sudo sed -i 's/\/rm\//\/en\//g' /etc/apache2/sites-available/org-dicoado-en-ssl.conf # Change "rm" to "en" everywhere
sudo sed -i 's/rm\\/en\\/g' /etc/apache2/sites-available/org-dicoado-en-ssl.conf # Change "rm" to "en" everywhere
sudo ln -s ../sites-available/org-dicoado-en-ssl.conf /etc/apache2/sites-enabled/org-dicoado-en-ssl.conf # Create a link to this file
sudo systemctl reload apache2.service
```

Create MariaDB database:
* Define some password for the future MariaDB user, for instance with `head -c 1000 /dev/urandom|sha1sum`
* Execute `sudo mysql` on the command line, then:
```
CREATE DATABASE dicoado_en;
CREATE USER dicoado_en@localhost IDENTIFIED BY 'password';
GRANT ALL ON dicoado_en.* TO dicoado@localhost;
```

Note: this "GRANT ALL" might be hardened.

An existing SQL dump might be imported with:
`cat dump.sql|mysql -udicoado_en -p dicoado_en`
`gunzip -c dump.sql.gz|mysql -udicoado_en -p dicoado_en`

Or an empty MediaWiki wiki might be created with:
```
cd /opt/mediawiki/1.39
php maintenance/install.php php --dbname=dicoado_en --dbuser=dicoado_en --dbpass=password --lang=en --pass=pass2 'Titre site' WikiSysop
```
(here WikiSysop is the first user with the password "pass2", the complete doc and parameters for this command is with `php maintenance/install.php -h`)

Configure basic parameters for MediaWiki:
* for each parameter in /etc/mediawiki/InitialiseSettings.php, set a new key "en" and the value of the parameter
* for each parameter in /etc/mediawiki/PrivateSettings.php, set a new key "en" and the value of the parameter (if the value for the key "default" is fine, you don’t need to overwrite it)
* add a new key "en" in /etc/mediawiki/versions-prod.php with the MediaWiki version (e.g. "1.39")

Open in a browser https://en.dicoado.org/dico/. If it works, it’s fine, gg.
Else you can set in InitialiseSettings.php the parameter wgDebugLogFile and wgShowDebug to see debug log.

Sharing of users
----------------

For subdomains of dicoado.org, the sharing of users and cross-wiki sessions with fr.dicoado.org should work (almost) automatically.

It is required to add the new language in the parameter 'wgSharedDB' (in /etc/mediawiki/InitialiseSettings.php); other parameters are configured to read-write users from fr.dicoado.org.

It is also required to give to the MariaDB user the rights to read/write inside the fr.dicoado.org database: execute `sudo mysql` then:
```
GRANT SELECT, UPDATE, INSERT ON nxxs_dicoado_fr.user TO dicoado_en@localhost;
GRANT SELECT, UPDATE, INSERT, DELETE ON nxxs_dicoado_fr.user_properties TO dicoado_en@localhost;
GRANT SELECT, UPDATE, INSERT ON nxxs_dicoado_fr.actor TO dicoado_en@localhost;
```


Maintenance
===========

Maintenance scripts
-------------------

To execute a maintenance script (e.g. eval) for a given language (e.g. rm.dicoado.org), the following commands are equivalent (the `.php` is optional and by default the script is search in `maintenance/` subdirectory):
```
mwscript --wiki=rm.dicoado.org maintenance/eval.php
mwscript --wiki=rm.dicoado.org eval.php
mwscript --wiki=rm.dicoado.org eval
```

For extensions, the complete path is necessary:
```
mwscript --wiki=rm.dicoado.org extensions/Echo/maintenance/generateSampleNotifications.php
```

The script mwscript is located in `/usr/local/bin/mwscript` and contains:
```
sudo -u www-data /usr/bin/php7.4 -d variables_order=EGPCS /opt/mediawiki/farm/bin/mwscript.php "$@"
```
