<?php

return [

  # Versions of the production wikis
  'central' => '1.39.8',
  'fr' => '1.39.8',
  #'de' => '1.39',
  #'it' => '1.39',
  'rm-vl' => '1.39.8',
  #'en' => '1.39',

];
