<?php

return [

  # General settings
  'wgSitename' => [
    'central' => 'Le Dico des Ados (central)',
    'fr' => 'Le Dico des Ados',
    'de' => 'Le Dico des Ados',
    'it' => '',
    'rm-vl' => 'Le Dico des Ados',
    'en' => '',
  ],
  'wgMetaNamespace' => [
    'central' => 'Dico',
    'fr' => 'Dico',
    'de' => 'Dico',
    'it' => '',
    'rm-vl' => '',
    'en' => '',
  ],
  'wgServer' => [
    'central' => 'http://central.dicoado.org',
    'fr' => 'https://fr.dicoado.org',
    'de' => 'https://de.dicoado.org',
    'it' => 'https://it.dicoado.org',
    'rm-vl' => 'https://rm-vl.dicoado.org',
    'en' => 'https://en.dicoado.org',
  ],
  'wgLogo' => [
    'fr' => "/w/images/thumb/c/c9/Logo.png/320px-Logo.png",
    'de' => '',
    'it' => '',
    'rm-vl' => "/w/images/thumb/c/c9/Logo.png/320px-Logo.png",
    'en' => '',
  ],
  'wgLanguageCode' => [
    'central' => 'en',
    'fr' => 'fr',
    'de' => 'de',
    'it' => 'it',
    'rm-vl' => 'rm',
    'en' => 'en',
  ],
  'wgUploadDirectory' => [
    'central' => '/home/www-dicoado/storage/central/images',
    'fr' => '/home/www-dicoado/storage/fr/images',
    'rm-vl' => '/home/www-dicoado/storage/rm-vl/images',
  ],

  # Cache
  'wgCacheDirectory' => [
    'central' => '/home/www-dicoado/storage/central/cache',
    'fr' => '/home/www-dicoado/storage/fr/cache',
    'de' => '/home/www-dicoado/storage/de/cache',
    'it' => '/home/www-dicoado/storage/it/cache',
    'rm-vl' => '/home/www-dicoado/storage/rm-vl/cache',
    'en' => '/home/www-dicoado/storage/en/cache',
  ],

  # Semantic MediaWiki settings
  'wgUseExtensionSemanticMediaWiki' => [
    'central' => true,
    'fr' => true,
    'de' => true,
    'it' => true,
    'rm-vl' => true,
    'en' => true,
  ],
  'smwgConfigFileDir' => [
    'central' => '/home/www-dicoado/storage/central/config',
    'fr' => '/home/www-dicoado/storage/fr/config',
    'de' => '/home/www-dicoado/storage/de/config',
    'it' => '/home/www-dicoado/storage/it/config',
    'rm-vl' => '/home/www-dicoado/storage/rm-vl/config',
    'en' => '/home/www-dicoado/storage/en/config',
  ],

  # Sharing of user table
  # https://www.mediawiki.org/wiki/Manual:Shared_database
  # All wikis except 'fr', which itself is the reference database
  'wgSharedDB' => [
    'central' => 'nxxs_dicoado_fr',
    'rm-vl' => 'nxxs_dicoado_fr',
  ],

  'wgSharedPrefix' => [
    'default' => '',
  ],

  '+wgSharedTables' => [
    'default' => [
      'actor',
    ],
  ],
  # The cookie settings should be shared amongst the different languages/sub-domains to enable cross-wiki sessions
  'wgCookieDomain' => [
    'default' => '.dicoado.org',
  ],
  'wgCookiePrefix' => [
    'default' => 'nxxs_dicoado_fr',
  ],
  'wgCookieSecure' => [
    'default' => true,
  ],
  'wgCookieSameSite' => [
    'default' => 'Lax',
  ],
  # It is important to share the keyspace amongst different wikis to enable server-side sharing of sessions (else there are small side effects)
  '+wgObjectCaches' => [
    'default' => [
      'memcached-php-sessions' => [
        'class' => MemcachedPhpBagOStuff::class,
        'loggroup' => 'memcached',
        'keyspace' => 'nxxs_dicoado_fr',
      ],
    ],
  ],
  'wgSessionCacheType' => [
    'default' => 'memcached-php-sessions',
  ],

  # DEBUG
  'wgDebugLogFile' => [
    #'central' => '/tmp/debug-central.log',
    #'fr' => '/tmp/debug-fr.log',
    #'rm-vl' => '/tmp/debug-rm.log',
  ],

  'wgShowDebug' => [
    #'fr' => true,
    #'rm-vl' => true,
  ],

  'wgUseExtensionGlobalUserrights' => [
    'default' => true,
  ],

];
