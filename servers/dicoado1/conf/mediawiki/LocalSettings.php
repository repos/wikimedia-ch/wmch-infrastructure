<?php
# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}
# Rififi 2022.06.11 moved from array() to [] to make it more readable.

// Guess if this is a demo, or not.
// If the filesystem is in the ".demo/ directory, have this constant to True.
// This constant may be read from the "secret" localsettings.
define( 'DICOADO_DEMO', strpos( __DIR__, '.demo/' ) !== false );

## The URL base path to the directory containing the wiki;
$wgScriptPath = "/w";
# Rififi 2022.06.11: removed unused feature
$wgArticlePath = "/dico/$1";
$wgUsePathInfo = false;

## Solution for Mediawiki login cancelled to prevent session hijacking
####COOKIES
#$wgCookieSecure = false; # Wiki Valley / Seb : TODO à remettre à true
#$wgCookieSameSite = "Lax"; # Vivian 07.05.21 : ne pas mettre "Strict" pour rester connecté quand on arrive d'ailleurs
#$wgCookieSameSite = "None"; # Vivian 07.05.21 : ne pas mettre "Strict" pour rester connecté quand on arrive d'ailleurs
#$wgCookieDomain = ".dicoado.org"; # Rififi 12.04: all subdomains (en., fr., de.): same login

#if( DICOADO_DEMO ) {
#	$wgCookiePrefix = "dicoado_demo"; // Wiki Valley / Seb / 2023-12-22 : pour ne pas mélanger les cookies avec la prod
#
#}

# Rififi 14.04 updated comment
# Rififi 2022.06.11: removed old script to delete cookies

# Rififi 09.04.2021: removed showExceptionDetails set to true, as it is a potential breach
# we will only use it in debug mode
### SECURITY
# Rififi 2022.06.11 moved this to the right place
$wgAllowSiteCSSOnRestrictedPages = true;

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;

# Rififi 2022.11.12 disabling non https in case of server failure
$wgForceHTTPS = true;

## UPO means: this is also a user preference option
$wgEnableEmail = true;
$wgEnableUserEmail = false; # UPO

#Rififi 23.12.21: dicoado.org, not ch.
# changed from 'admin' to 'info' 2024-07-18 -- [[User:ValerioBozz-WMCH]]
// Mailbox Documentation: -- [[User:ValerioBozz-WMCH]]
// https://members.wikimedia.ch/wiki/Infrastructure/Emails#Mailboxes_in_dicoado.org
$wgEmergencyContact = "info@dicoado.org";
$wgPasswordSender = "info@dicoado.org";

$wgEnotifUserTalk = true; # UPO
$wgEnotifWatchlist = true; # UPO
$wgEmailAuthentication = true;

#######PERFORMANCES
#CACHE SETTINGS Shared memory settings. php updateSpecialPages.php
#$wgUseFileCache = true; # Wiki Valley / Seb / 2023-12-05 - heavily-deprecated cache mechanism
$wgCacheEpoch = 20240223105725;
$wgMainCacheType =
        $wgMessageCacheType = 
        $wgParserCacheType =
        $wgLanguageConverterCacheType = CACHE_MEMCACHED;
//      $wgSessionCacheType = CACHE_DB;
// Rififi 23.12.21 we would like to use memcached but unfortunately we can't --*
// boz    2024-02-22 now we have memcached :D
// https://www.mediawiki.org/wiki/Manual:Memcached
$wgMemCachedServers = [ '127.0.0.1:11211' ];

$wgFooterLinkCacheExpiry = 86400;
$wgInvalidateCacheOnLocalSettingsChange = true; // Wiki Valley / Seb / 2023-12-08 : changé de false à true, au moins pour le debug
# Invalidate only if new extension
//$wgExtensionInfoMtime = filemtime( "$IP/Config/SkinsExtensions.php" );
// The previous line was a bit non-standard. Maybe it's better if we just point to the settings,
// so if we add an extension, we clear the cache.
// https://www.mediawiki.org/wiki/Manual:$wgExtensionInfoMTime
$wgExtensionInfoMtime = filemtime( "/var/cache/mediawikifarm/LocalSettings/rm-vl.dicoado.org.php" );
## Set $wgCacheDirectory to a writable directory on the web server
## to make your wiki go slightly faster. The directory should not
## be publically accessible from the web.
$wgMaxShellMemory = 614400;
$wgMiserMode = true;
$wgShowArchiveThumbnails = false;


#######MEDIAS
#UPLOADS
$wgEnableUploads = true;
# Rififi 2022.06.11 changed max file size to 25MiB, 300 MiB is waay too large
$wgUploadSizeWarning = 26214400; 
$wgMaxUploadSize = 26214400;
#$wgGenerateThumbnailOnParse = false;
#$wgUseImageMagick = false;
$wgUseImageMagick = true;
$wgUseImageResize = true;
$wgAllowTitlesInSVG = true;

#$wgImageMagickConvertCommand = "/usr/bin/convert";
$wgFileExtensions = array_merge(
    $wgFileExtensions,
    [
        'svg', 'ogg', 'mp4', 'webm', 'mov', 'mp3', 'pdf'
    ]
);
# Vivian 30.04.2021 : SVG Rendering issues
# Valerio Bozzolan (presumed 2023.09.12): commented to adopt default
# Rififi (2023.09.12): rsvg back again, ImageMagick is of poor quality
$wgSVGConverter = 'rsvg';
## If you use ImageMagick (or any other shell command) on a
## Linux server, this will need to be set to the name of an
## available UTF-8 locale
$wgShellLocale = "C.UTF-8";#Technically useless except if "any other shell command" is used
# InstantCommons allows wiki to use images from https://commons.wikimedia.org
$wgUseInstantCommons = true;

## Custom names for core namespaces
$wgExtraNamespaces[NS_USER]          = "Dicographe";
$wgExtraNamespaces[NS_USER_TALK]          = "Discussion_dicographe";
#######CUSTOM NAMESPACES
##NS_THESAURUS (Champ lexical:)
define("NS_THESAURUS", 3000);
define("NS_THESAURUS_TALK", 3001);

$wgExtraNamespaces[NS_THESAURUS]	  = "Champ_lexical";
$wgExtraNamespaces[NS_THESAURUS_TALK] = "Discussion_champ_lexical";
$wgNamespacesWithSubpages[NS_THESAURUS]		 = true;
$wgNamespacesWithSubpages[NS_THESAURUS_TALK] = true;

$wgNamespacesToBeSearchedDefault[NS_THESAURUS] = true;
####NAMESPACE ALIASES
$wgNamespaceAliases = [
	'Discussion_Dicographe' => NS_USER_TALK,
	'MW' => NS_MEDIAWIKI,
	'Discussion_MW' => NS_MEDIAWIKI_TALK,
	'Cat' => NS_CATEGORY,
	'Discussion_Cat' => NS_CATEGORY_TALK,
	'Prop' => 102,
	'Discussion_Prop' => 103,
	'M' => NS_TEMPLATE,
	'Discussion_M' => NS_TEMPLATE_TALK,

	'Thésaurus' => NS_THESAURUS,
	'Discussion_thésaurus' => NS_THESAURUS_TALK,
	'CL' => NS_THESAURUS,
	'Discussion_CL' => NS_THESAURUS_TALK,
	'Idée' => NS_THESAURUS,
	'Discussion_Idée' => NS_THESAURUS_TALK,
	'Thesaurus' => NS_THESAURUS,
	'Thesaurus_talk' => NS_THESAURUS_TALK
];



#####LICENSE
## For attaching licensing metadata to pages
$wgRightsPage = "Dico:Copyrights"; # Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl = "https://creativecommons.org/licenses/by-sa/4.0/deed.fr";
$wgRightsText = "CC BY-SA 4.0";
$wgRightsIcon = null;
# Path to the GNU diff3 utility. Used for conflict resolution.
#$wgDiff3 = "";



#######SKINS
# Enabled skins.
//wfLoadSkin('Vector');
wfLoadSkin( 'foreground' );
#Default skin
$wgDefaultSkin = "foreground";
$wgForegroundFeatures = [
  'showActionsForAnon' => false,
  'navbarIcon' => true,
  'enableTabs' => true,
  'wikiName' => '',
  'autocapitalizeSearchBar' => 'off'
];
# Rififi 23.12.21: Foreground is specific, so we don't enable any other skin.



#######MISC OPTIONS
####DEFAULT USER OPTIONS
$wgDefaultUserOptions['rememberpassword'] = 1;
$wgDefaultUserOptions['usenewrc'] = 0; #ne pas grouper les changements par page dans les modifications récentes et la liste de suivi

####CATEGORIES SORTING ALGORITHMS
$wgCategoryCollation = "uca-fr";

####NO CAPITAL LINKS
#PuTTY : php cleanupCaps.php
$wgCapitalLinkOverrides[ NS_MAIN ] = false;
$wgRestrictDisplayTitle = false;
//$wgCapitalLinks = false;
####SITEMAP
# PuTTY SSH : php generateSitemap.php --skip-redirects
# Rififi 2022.06.11 using NS_* to make it more readable
$wgSitemapNamespaces = [ NS_MAIN, NS_PROJECT, NS_HELP, NS_CATEGORY, NS_THESAURUS ];

####SIGNATURE IN DICO:
# Rififi 2022.06.11 using NS_* to make it more readable
$wgExtraSignatureNamespaces = [ NS_PROJECT ];

####DISABLING JOBS ASYNC
$wgRunJobsAsync = true;

####REMOVE POWERED BY MEDIAWIKI AND SEMANTIC
unset( $wgFooterIcons['poweredby'] );
# unset( $wgFooterIcons['copyright'] );
$wgFooterIcons['poweredby']['semanticmediawiki'] = false;

####SHOW CREDITS
$wgMaxCredits = 0;

####MAKE EXTERNAL LINKS OPEN IN A NEW WINDOW
$wgExternalLinkTarget = '_blank';

####DEBUGGIN - DEVELOPPING - LOCKING Rififi 09.04.2021 : updating this section, removing old stuff and putting the right one
# $wgReadOnly = 'Mise à jour en cours, Rififi.'; # Just locks the database but site still accessible, may be useful.
#$wgReadOnly = "Migration in progress. See https://phabricator.wikimedia.org/T305080";
# To put a nice maintenance page and allow only some IPs, use Infomaniak (contact Vivian or Rififi)

# put a cookie on your browser with a secret value that you fill here to be the only one with some fancy configuration
# use httpOnly and secure and set path and domain properly, as well as samesite if possible (and take care that no external script could interject)
# (don't forget to disable it/clear the token/change the cookie name if necessary)
// if (defined('MW_DB') || (isset($_COOKIE['token']) && hash_equals($_COOKIE['token'], "CHANGE_ME_fezr4f8dfgh0j2dg5fhjo8dsvc55df6h6gj8yiu4o65hf10sxv60xdr4gf65g4df6gfh5"))) {
//	# Rififi 2023.07.13 wgDebug does not exist ><
//	# Rififi 2023.09.10 Reorganized a bit this sections

//	# Logging - FILES LISTED HERE MUST BE INNACCESSIBLE TO THE WEB
//	$wgLanguageCode = "en";
//	$wgDebugLogFile = "/var/www/dicoado/fr/private/log/debug.log";
//	# Too spammy hence separating
//	$wgDebugLogGroups = [
//		"DBQuery"  => '/var/www/dicoado/fr/private/log/dbquery.log'
//	];

//	# Various configurations in order to make MediaWiki more verbose.
//	# This can be in the logs or on site directly (shoutout to the debug toolbar)
//	# HENCE THE NEED TO PROPERLY SECURE THESE OPTIONS BEHIND AN AUTHENTICATION METHOD
//	$wgDebugDumpSql = true;
//	$wgDebugToolbar = true;
//	$wgShowDBErrorBacktrace = true;
//	$wgShowDebug = true;
//	$wgShowExceptionDetails = true;
//	$wgShowSQLErrors = true;

//	# Forcing errors to be displayed and to be all reported
//	# HENCE THE NEED TO PROPERLY SECURE THESE OPTIONS BEHIND AN AUTHENTICATION METHOD
//	$wgDevelopmentWarnings = true;
//	ini_set("display_errors", 1);
//	ini_set("error_reporting", E_ALL);

//	# Disabling the cache, this permits avoiding leaking data and also to see immediately changes made on the server.
//	$wgUseFileCache        = false;
//	$wgMainCacheType       = CACHE_NONE;
//	$wgUseLocalMessageCache= false;
//	$wgParserCacheType     = CACHE_NONE;
//	# This must be still enabled otherwise no session may work.
//	$wgSessionCacheType   = CACHE_DB;
//} else { # For normal users only (not affecting you), if you want to put special configs during maintenance (wgReadOnly for example)
//}

#To stop the cache from working, so you can immediately see changes (but it slows down the server)
// if(true) { #just put false to quickly reenable it
	// $wgUseLocalMessageCache= false;
	// $wgUseFileCache        = false;
	// $wgParserCacheType     = CACHE_NONE;
	// $wgMainCacheType       = CACHE_NONE;
	// $wgSessionCacheType   = CACHE_NONE;
// }
#You can note any temporary change below to easily revoke them
# TODO: check include ip on ContactPage
# TODO: talk about GA and Matomo
# TODO: talk about allowing CSS on restricted
# TODO: cron, check the way sh files are included
# TODO: check MEssages*.php, if needed and what happen in other languages
# TODO: check bug fixed SMW

###TIMEZONE
$wgLocaltimezone = "Europe/Paris";
date_default_timezone_set( $wgLocaltimezone );


#######EXTENSIONS
##THAT NEEDS CONFIGURATION
#Semantics and related (PageForms and SemanticDrillDown) - put first cause other extensions may need it
#2022.06.11 Rififi updated SMW
wfLoadExtension( 'SemanticMediaWiki' );
enableSemantics( 'https://dicoado.org', true );

# $smwgChangePropagationProtection = false;
$smwgEditProtectionRight = "smw-pageedit";
$smwgEnabledEditPageHelp = false;
$smwgEnabledFulltextSearch = true;
# Rififi and Vivian edited 23.04.21: hidding factbox when previewing, disabling this feature everywhere
$smwgParserFeatures = SMW_PARSER_STRICT | SMW_PARSER_INL_ERROR | SMW_PARSER_HID_CATS | SMW_PARSER_LINV;
$smwgQMaxInlineLimit = 5000;
$smwgShowFactboxEdit = $smwgShowFactbox = SMW_FACTBOX_HIDDEN;
$smwgNamespacesWithSemanticLinks = [
	NS_MAIN => true,
	NS_TALK => false,
	NS_USER => true,
	NS_USER_TALK => false,
	NS_PROJECT => true,
	NS_PROJECT_TALK => false,
	NS_FILE => true,
	NS_FILE_TALK => false,
	NS_MEDIAWIKI => false,
	NS_MEDIAWIKI_TALK => false,
	NS_TEMPLATE => false,
	NS_TEMPLATE_TALK => false,
	NS_HELP => true,
	NS_HELP_TALK => false,
	NS_CATEGORY => true,
	NS_CATEGORY_TALK => false,
	102 => true
];

wfLoadExtension( 'PageForms' );
$wgPageFormsLinkAllRedLinksToForms = true;
# Rififi 2022.06.11 moved settings to the right place
$wgPageFormsHeightForMinimizingInstances = -1;

#require_once '/var/www/dicoado/fr/private/PrivateSettings.php';

#Cargo Rififi 10.04 updated
wfLoadExtension( 'Cargo' );
#$wgCargoDBtype = $dicoCargoDBtype;
#$wgCargoDBserver = $dicoCargoDBserver;
#$wgCargoDBname = $dicoCargoDBname;
#$wgCargoDBuser = $dicoCargoDBuser;
#$wgCargoDBpassword = $dicoCargoDBpassword;
#$wgCargoDBprefix = $dicoCargoDBprefix;
$smwgEnabledCompatibilityMode = true;
$wgCargoDecimalMark = ",";
$wgCargoDigitGroupingCharacter = " ";
$wgCargoDrilldownMinValuesForComboBox = 100;
$wgCargo24HourTime = true;
$wgCargoDrilldownSmallestFontSize = 10;
$wgCargoDrilldownLargestFontSize = 30;
$wgCargoDBRowFormat = 'COMPRESSED';
$wgCargoPageDataColumns[] = 'categories';
$wgCargoPageDataColumns[] = 'creator';
$wgCargoPageDataColumns[] = 'creationDate';
$wgCargoPageDataColumns[] = 'modificationDate';
$wgCargoPageDataColumns[] = 'numRevisions';
$wgCargoPageDataColumns[] = 'isRedirect';
$wgCargoPageDataColumns[] = 'fullText';

#CirrusSearch
#wfLoadExtension( 'CirrusSearch' );
#$wgDisableSearchUpdate = true; #21.05 Vivian a désactivé la variable en attendant que Rififi vienne me sauver

# Google Analytics Integration : still uses require_once (comment Rififi 10.04)
#require_once "$IP/extensions/googleAnalytics/googleAnalytics.php";
# Replace xxxxxxx-x with YOUR GoogleAnalytics UA number
$wgGoogleAnalyticsAccount = 'UA-49598591-1';
# Add HTML code for any additional web analytics (can be used alone or with $wgGoogleAnalyticsAccount)
$wgGoogleAnalyticsOtherCode = '<script>ga(\'create\', \'UA-49598591-1\', {\'siteSpeedSampleRate\': 50});</script>';
# Optional configuration (for defaults see googleAnalytics.php)
# Store full IP address in Google Universal Analytics (see https://support.google.com/analytics/answer/2763052?hl=en for details)
# 2022.06.16 Rififi: this ensures confidentiality while still be allowed to know the location
$wgGoogleAnalyticsAnonymizeIP = true; 
# Array with special pages where web analytics code should NOT be included.
$wgGoogleAnalyticsIgnoreSpecials = [ 'Preferences', 'ChangePassword', 'OATH' ];

# GTag (Google Analytics)
//wfLoadExtension( 'GTag' );
// disabled in 2023-10-27
# 2022.06.16 Rififi: this ensures confidentiality while still be allowed to know the location
$wgGTagAnonymizeIP = true;
$wgGTagAnalyticsId = 'G-3GZDPF4NFC'; // replace with your GA id
// $wgGTagTrackSensitivePages = false;

# GoogleRichCards Rififi 10.04 updated, extension now supports rich cards for other things than articles
// Unfortunately it's not supported since MediaWiki 1.39
// https://phabricator.wikimedia.org/T348426#9305177
//wfLoadExtension( 'GoogleRichCards' );
$wgGoogleRichCardsAnnotateArticles = true;
$wgGoogleRichCardsAnnotateEvents = false;
$wgGoogleRichCardsAnnotateWebSite = false;

#Contact page extension
# Rififi&Vivian disabled it 2022.08.06
# wfLoadExtension( 'ContactPage' );
$wgContactConfig['default'] = [
	'RecipientUser' => 'Vivian', #Must be the name of a valid account which also has a verified e-mail-address added to it.
	'SenderName' => 'Formulaire de contact de ' . $wgSitename, #"Contact Form on" needs to be translated
  	'SenderEmail' => null, #Defaults to $wgPasswordSender, may be changed as required
	'RequireDetails' => true, #Either "true" or "false" as required
	'IncludeIP' => true, #Either "true" or "false" as required
	'MustBeLoggedIn' => true, // Check if the user is logged in before rendering the form
	'AdditionalFields' => [
		'Text' => [
			'label-message' => 'emailmessage',
			'type' => 'textarea',
			'rows' => 20,
			'required' => true,  #Either "true" or "false" as required
		],
	],
	'RLModules' => [],  // Resource loader modules to add to the form display page.
	'RLStyleModules' => [],  // Resource loader CSS modules to add to the form display page.
];

#HeaderTabs
wfLoadExtension( 'HeaderTabs' );
$wgHeaderTabsEditTabLink = false;
$wgHeaderTabsStyle = 'retro';

#HeadScript (favicon and script)
wfLoadExtension( 'HeadScript' ); # Rififi 10.04 updated using wfLoadExtension
$wgHeadScriptCode = <<<'START_END_MARKER'
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="194x194" href="/favicon-194x194.png">
<link rel="icon" type="image/png" sizes="192x192" href="/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#e77f1c">
<meta name="apple-mobile-web-app-title" content="Dico des Ados">
<meta name="application-name" content="Dico des Ados">
<meta name="msapplication-TileColor" content="#2b5797">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#333333">
  
<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(["setDomains", ["*.dicoado.org","*.fr.dicoado.org"]]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://matomo.dicoado.org/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '2']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!--<noscript><p><img src="https://matomo.dicoado.org/matomo.php?idsite=2&rec=1" style="border:0;" alt="" /></p></noscript>-->
<!-- End Matomo Code -->
START_END_MARKER;

#RegexFun Rififi 10.04 : updated, still uses require_once
// This extension was crashing
// https://www.mediawiki.org/wiki/Extension_talk:Regex_Fun
// Notice: Indirect modification of overloaded property Parser::$mExtRegexFun
require_once "$IP/extensions/RegexFun/RegexFun.php";
$egRegexFunMaxRegexPerParse = 10;

#ParserFunctions
wfLoadExtension( 'ParserFunctions' );
$wgPFEnableStringFunctions = true;

# PDFEmbed
wfLoadExtension( 'PDFEmbed' );

#Thanks
wfLoadExtension( 'Thanks' );
$wgThanksConfirmationRequired = false;

#Time Media Handler
wfLoadExtension( 'TimedMediaHandler' );
$wgFFmpegLocation = '/usr/bin/ffmpeg';

#The total amout of time a transcoding shell command can take:
$wgEnableTranscode = false;
$wgTranscodeBackgroundTimeLimit = 0;
#Maximum amount of virtual memory available to transcoding processes in KB
$wgTranscodeBackgroundMemoryLimit = 2 * 1024 * 1024; #2GiB avconv, ffmpeg2theora mmap resources so virtual memory needs to be high enough
#Maximum file size transcoding processes can create, in KB

#$wgOggThumbLocation = false; #use ffmpeg for performance
# use new ffmpeg build w/ VP9 & Opus support

# The type of HTML5 player to use ('videojs' and 'mwembed' are other options)
# Rififi 2022.06.11 removed variable that didn't even exist
# Rififi 2022.06.12 using videojs
# Vivian 2022.06.18 back to mwembed ; videojs would be nice if we don't have popup for sounds
$wgTmhWebPlayer = "mwembed";
# Enable the Beta Feature for trying out the new video player (see also the BF whitelist)
#$wgTmhUseBetaFeatures = true;

#Wikieditor
wfLoadExtension( 'WikiEditor' );
# Enables use of WikiEditor by default but still allows users to disable it in preferences
$wgDefaultUserOptions['usebetatoolbar'] = 1;
# Enables link and table wizards by default but still allows users to disable them in preferences
$wgDefaultUserOptions['usebetatoolbar-cgd'] = 1;
# Displays the Preview and Changes tabs
$wgDefaultUserOptions['wikieditor-preview'] = 0;
# Displays the Publish and Cancel buttons on the top right side
$wgDefaultUserOptions['wikieditor-publish'] = 0;

#VisualEditor
wfLoadExtension( 'VisualEditor');
wfLoadExtension( 'VEForAll' );
//wfLoadExtension( 'Parsoid', __DIR__ . '/vendor/wikimedia/parsoid/extension.json' ); # Wiki Valley / Seb / 2023-12-08 : needed for VEForAll
wfLoadExtension( 'Parsoid', "$IP/vendor/wikimedia/parsoid/extension.json" ); # Wiki Valley / Seb / 2023-12-08 : needed for VEForAll
// 2024-02-29: Parsoid commented, probably not necessary anymore after 1.35 - Valerio_Bozzolan
//             https://www.mediawiki.org/wiki/Extension:Parsoid
// 2024-03-04: Parsoid un-commented but fixed - Valerio_Bozzolan

#Scribunto
wfLoadExtension( 'Scribunto' );
$wgScribuntoDefaultEngine = 'luastandalone';

#AbuseFilter
wfLoadExtension( 'AbuseFilter' );
$wgAbuseFilterActions['block'] = $wgAbuseFilterActions['degroup'] = false;
$wgAbuseFilterLogPrivateDetailsAccess = true;
$wgAbuseFilterPrivateDetailsForceReason = true;
$wgAbuseFilterLogIPMaxAge = 7689600;
#2022-05-10 Rififi stop flooding RC, in fact I wanted it to just show about filter edition, not abuselog
#$wgAbuseFilterNotifications = 'rc';

##NO CONFIGURATION
#Load extensions
wfLoadExtension( 'Arrays' ); # Rififi 10.04 updated, using wfLoadExtension
wfLoadExtension( 'CategoryTree' );
wfLoadExtension( 'CheckUser' ); # Rififi 2023.04.05 updated
wfLoadExtension( 'CharInsert' );
wfLoadExtension( 'cldr' );
wfLoadExtension( 'Echo' );
#wfLoadExtension( 'Elastica' );
wfLoadExtension( 'Gadgets' );
#wfLoadExtension( 'HorsesGenerator' ); // à activer les 1er avril, penser à c/c le dossier sans le .git depuis Private
wfLoadExtension( 'InputBox' );
wfLoadExtension( 'MetaDescriptionTag' );
wfLoadExtension( 'MagicNoCache' );
wfLoadExtension( 'MultimediaViewer' );
wfLoadExtension( 'NoCat' );
wfLoadExtension( 'Nuke' ); # Rififi 10.04 using wfLoadExtension
# Vivian 10.04.21 : removing PageTriage
wfLoadExtension( 'PageImages' );
#$wgPageImagesScores['position'][0] = 1000; (ça donne la 1ère image de la galerie !?)
#$wgPageImagesScores['galleryImageWidth'] = ['"99":-100, "100":-120']; # Vivian 14.05 je sais pas comment faire
wfLoadExtension( 'PdfHandler' ); #Vivian 22.03.2023 à désactiver en cas d'erreur upload PDF
# Rififi 2023.06.08: this functionnality is apparently broken
#$wgPdfCreateThumbnailsInJobQueue = true;
wfLoadExtension( 'Renameuser' );
# Rififi 19.04.21 mise à jour manuelle, pour réparer un bug
wfLoadExtension( 'ReplaceText' );
wfLoadExtension( 'RandomSelection' );
wfLoadExtension( 'ThrottleOverride' );
wfLoadExtension( 'TitleBlacklist' );
wfLoadExtension( 'UserMerge' );
wfLoadExtension( 'Variables' );
wfLoadExtension( 'SyntaxHighlight_GeSHi' );
wfLoadExtension( 'TemplateData' );
#Rififi removed DarkMode 12.07: buggy
##NOTE
#Antispam is in the PrivateSetting file (because there is a credential in its configuration).

#######PERMISSIONS
####PROTECTED NAMESPACES
$wgNamespaceProtection[NS_CATEGORY] = [ 'edit-categorypages' ];

####RESTRICTIONS LEVELS (to add a protection level on pages)
$wgRestrictionLevels[] = 'pupils-and-teachers';
$wgRestrictionLevels[] = 'autopatrol-protect'; 

####DIRECT PERMISSIONS
##ALL
$wgGroupPermissions['*']['viewedittab'] = false;
#GoogleAnalytics
$wgGroupPermissions['*']['noanalytics'] = false; #Use this permission to exclude specific user groups from web analytics. Here it's set to false, normal users will be targeted by analytics.
#extension :Je EmbedPDF
$wgGroupPermissions['*']['embed_pdf'] = true;

##USERS (Utilisateurs)
$wgGroupPermissions['user']['move-categorypages'] = false;
$wgGroupPermissions['user']['move-rootuserpages'] = false;
$wgGroupPermissions['user']['move-subpages']      = false;
$wgGroupPermissions['user']['editcontentmodel']   = false;
#Adding or removing group
$wgGroupsAddToSelf['user'][] = 'wikitext-editor';
$wgGroupsRemoveFromSelf['user'][] = 'wikitext-editor';
#EXTENSIONS: GoogleAnalytics
$wgGroupPermissions['user']['noanalytics'] = false;
$wgGroupPermissions['user']['gtag-exempt'] = false;
#PageForms
$wgGroupPermissions['user']['multipageedit'] = false;

##AUTOCONFIRMED (Utilisateur autoconfirmés)
#EXTENSIONS: CleanTalk
$wgGroupPermissions['autoconfirmed']['cleantalk-bypass'] = true; #Skip test for autoconfirmed users

##AUTOPATROL
$wgGroupPermissions['autopatrol']['autopatrol']         = true;
$wgGroupPermissions['autopatrol']['autopatrol-protect'] = true;
#EXTENSIONS: PageForms
$wgGroupPermissions['autopatrol']['multipageedit']      = true;

##INVIS
#EXTENSIONS: GoogleAnalytics
$wgGroupPermissions['invis']['noanalytics'] = true;
$wgGroupPermissions['invis']['gtag-exempt'] = true;

##PATROLLERS (Patrouilleurs)
$wgGroupPermissions['patroller']['editinterface']      = true;
$wgGroupPermissions['patroller']['movefile']           = true;
$wgGroupPermissions['patroller']['move-subpages']	   = true;
$wgGroupPermissions['patroller']['move-rootuserpages'] = true;
$wgGroupPermissions['patroller']['patrol']             = true;
$wgGroupPermissions['patroller']['protect']            = true;
$wgGroupPermissions['patroller']['rollback']           = true;
$wgGroupPermissions['patroller']['suppressredirect']   = true;
#Adding and removing groups
$wgAddGroups['patroller'][] = 'autopatrol'; # Allow patrollers to add the autopatrol group...
$wgRemoveGroups['patroller'][] = 'autopatrol'; # And to remove it by the way

##PUPILS (Élèves)
$wgGroupPermissions['pupil']['pupils-and-teachers'] = true; # gives the "pupils-and-teachers" permission to pupils (pupils will be able to protect or edit pages to/with this protection level)

##WIKITEXT EDITORS (Éditeurs wikitexte)

$wgGroupPermissions['wikitext-editor']['viewedittab']= true;

##TEACHERS (Profs)
$wgGroupPermissions['teacher']['createaccount']      = true;
$wgGroupPermissions['teacher']['delete']             = true;
$wgGroupPermissions['teacher']['pupils-and-teachers']= true;
$wgGroupPermissions['teacher']['patrol']             = true;
$wgGroupPermissions['teacher']['protect']            = true;
$wgGroupPermissions['teacher']['rollback']           = true;
$wgGroupPermissions['teacher']['suppressredirect']   = true;
$wgGroupPermissions['teacher']['undelete']           = true;
$wgGroupPermissions['teacher']['unwatchedpages']     = true;
#Adding and removing groups
$wgAddGroups['teacher'] = [ 'pupil' ];
$wgRemoveGroups['teacher'] = [ 'pupil' ];

##ADMINISTRATORS (Administrateurs)
$wgGroupPermissions['administrator']['block']               = true;
$wgGroupPermissions['administrator']['delete']              = true;
$wgGroupPermissions['administrator']['deletedhistory']      = true;
$wgGroupPermissions['administrator']['deleterevision']      = true;
$wgGroupPermissions['administrator']['edit-categorypages']  = true;
$wgGroupPermissions['administrator']['editcontentmodel']    = true;
$wgGroupPermissions['administrator']['editprotected']       = true;
$wgGroupPermissions['administrator']['editrestrictedfields']= true;
$wgGroupPermissions['administrator']['ipblock-exempt']      = true;
$wgGroupPermissions['administrator']['mergehistory']        = true;
$wgGroupPermissions['administrator']['move-categorypages']  = true;
$wgGroupPermissions['administrator']['pupils-and-teachers'] = true;
$wgGroupPermissions['administrator']['titleblacklistlog']   = true;
$wgGroupPermissions['administrator']['unblockself']         = true;
$wgGroupPermissions['administrator']['undelete']            = true;
#Adding and removing groups
$wgAddGroups['administrator'] = [ 'pupil', 'wikitext-editor' ];
$wgRemoveGroups['administrator'] = [ 'pupil', 'wikitext-editor' ];

##ADMINISTRATORS +
$wgGroupPermissions['sysop']['deletelogentry'] = true;
$wgGroupPermissions['sysop']['smw-admin']      = true;
$wgGroupPermissions['sysop']['smw-pageedit']   = true;
$wgGroupPermissions['sysop']['smw-patternedit']= true;
$wgGroupPermissions['sysop']['smw-schemaedit'] = true;
#EXTENSIONS: ThrottleOverride
$wgGroupPermissions['sysop']['throttleoverride'] = true;
#AbuseFilter
$wgGroupPermissions['sysop']['abusefilter-hide-log'] = true;
$wgGroupPermissions['sysop']['abusefilter-hidden-log'] = true;

##ROBOTS
$wgGroupPermissions['bot']['noratelimit'] = true;
#EXTENSIONS: GoogleAnalytics
$wgGroupPermissions['bot']['noanalytics'] = true;
$wgGroupPermissions['bot']['gtag-exempt'] = true;
$wgGroupPermissions['bot']['cleantalk-bypass'] = true;

##BUREACURATS
$wgGroupPermissions['bureaucrat']['hideuser']        = true;
$wgGroupPermissions['bureaucrat']['suppressrevision']= true;
$wgGroupPermissions['bureaucrat']['suppressionlog']  = true;
#EXTENSIONS: UserMerge
$wgGroupPermissions['bureaucrat']['usermerge']       = true; #By default no one can use the "usermerge" special page, we want to enable it for bureaucrats

##CHECKUSERS
#EXTENTIONS: AbuseFilter
$wgGroupPermissions['checkuser']['abusefilter-log-detail'] = true;
$wgGroupPermissions['checkuser']['abusefilter-log-private'] = true;
$wgGroupPermissions['checkuser']['abusefilter-privatedetails'] = true;
$wgGroupPermissions['checkuser']['abusefilter-privatedetails-log'] = true;

####DELETED RIGHTS
$wgExtensionFunctions[] = function() use ( &$wgGroupPermissions, &$wgRevokePermissions, &$wgAddGroups, &$wgRemoveGroups, &$wgGroupsAddToSelf, &$wgGroupsRemoveFromSelf) {
    foreach(['smwadministrator','smwcurator','smweditor', 'suppress'] as $group){
        unset( $wgGroupPermissions[$group] );
        unset( $wgRevokePermissions[$group] );
        unset( $wgAddGroups[$group] );
        unset( $wgRemoveGroups[$group] );
        unset( $wgGroupsAddToSelf[$group] );
        unset( $wgGroupsRemoveFromSelf[$group] );
    }
};

####PERMISSIONS RELATED SETTINGS
##AUTO CONFIRM USERS WHEN....
$wgAutoConfirmAge = 259200;# 60*60*24*3: 3 days.
$wgAutoConfirmCount = 5;

##ALLOWS USER'S JS/CSS
$wgAllowUserJs   = true;
$wgAllowUserCss  = true;

####AUTOPROMOTING
# Rififi 2022.06.11 reorganised to make it more readable, added comment
#Bureaucrat includes Sysop which includes Administrator which includes Patroller which includes Autopatrol. Teacher includes autopatrol
$wgAutopromote['sysop']         = [ APCOND_INGROUPS, 'bureaucrat' ];
$wgAutopromote['administrator'] = [
    '|',
        [ APCOND_INGROUPS, 'sysop' ],
        [ APCOND_INGROUPS, 'bureaucrat' ]
];
$wgAutopromote['patroller'] = [
    '|',
        [ APCOND_INGROUPS, 'administrator' ],
        [ APCOND_INGROUPS, 'sysop' ],
        [ APCOND_INGROUPS, 'bureaucrat' ]
];
$wgAutopromote['autopatrol'] = [
    '|',
        [ APCOND_INGROUPS, 'patroller' ],
        [ APCOND_INGROUPS, 'teacher' ],
        [ APCOND_INGROUPS, 'administrator' ],
        [ APCOND_INGROUPS, 'sysop' ],
        [ APCOND_INGROUPS, 'bureaucrat' ]
];
#Bucreaucrat and Sysop both include interface-admin
$wgAutopromote['interface-admin'] = [
    '|',
        [ APCOND_INGROUPS, 'sysop' ],
        [ APCOND_INGROUPS, 'bureaucrat' ]
];
#Wikitext editors = all admins
$wgAutopromote['wikitext-editor'] = [
    '|',
        [ APCOND_INGROUPS, 'interface-admin' ],
        [ APCOND_INGROUPS, 'administrator' ],
        [ APCOND_INGROUPS, 'sysop' ],
        [ APCOND_INGROUPS, 'bureaucrat' ]
];

# Rififi 2022.06.11: removed temporary fix after we upgraded to 1.35.6


# Rififi 2023.09.10 moved the debug parameters to the appropriate DEBUG section in this file, guarded around a security check.
# Also disabling permanent debug log which stores a lot of informations while being unusable due to large amount of data.
#$wgDebugLogFile = "/var/log/mediawiki-debug-dicoado.log"; // Wiki Valley / Seb / 2023-12-08 : commenté pour éviter de faire grossir le fichier en question si c’est inutile

// Now we have a systemd job that does this. So, we can speedup
// the whole MediaWiki to ignore this.
// This is nearly zero but not zero, so, in case of problems with
// the background jobs, we still do them sometime, if any. -- [[User:Valerio Bozzolan]]
// https://www.mediawiki.org/wiki/Manual:$wgJobRunRate
$wgJobRunRate = 0.01;

#######SECURITY
# $wgSecretKey in PrivateSettings.php
# Changing this will log out all existing sessions.
$wgAuthenticationTokenVersion = "1";
# Site upgrade key. Must be set to a string (default provided) to turn on the
# web installer while LocalSettings.php is in place
# $wgUpgradeKey in PrivateSettings.php
#Forbidden usernames
$wgReservedUsernames[] = 'Flow talk page manager';
#Rififi 2021.05.19 deleted useless x-frame-options variable since we add them through .htheaders.inc.php; cleaned useless wgBreakFrames

#Antispam
wfLoadExtension( 'Antispam' );
# $wgCTAccessKey in PrivateSettings.php
$wgCTShowLink = false;

// Do not index a demo.
if( DICOADO_DEMO ) {
	$wgDefaultRobotPolicy = 'noindex,nofollow';

	// Maybe better to do these tests just in the demo :)
	//$wgDebugDumpSql = true;
	//$wgShowSQLErrors = true;
	//$wgShowDBErrorBacktrace = true;
}

#$wgResourceLoaderDebug = true; // Wiki Valley / Seb / 2023-12-08 : changé de true à (commenté=false)
$wgShowExceptionDetails = true;
#$wgShowDebug = true; // Wiki Valley / Seb / 2023-12-08 : changé de true à (commenté=false)
#$wgDevelopmentWarnings = true; // Wiki Valley / Seb / 2023-12-08 : changé de true à (commenté=false)

ini_set( "error_reporting", 0 );
ini_set( "display_errors", 0 );
