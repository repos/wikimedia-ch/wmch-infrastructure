<?php

return [

  # Production
  'dicoado' => [
    'server' => '(?P<lang>central|fr|de|it|rm-vl|en)\\.dicoado\\.org',
    'variables' => [
      [
        'variable' => 'lang',
        'file' => 'versions-prod.php',
      ],
    ],
    'suffix' => 'wiki',
    'wikiID' => '$langwiki',
    'HTTP404' => '/home/www-dicoado/web-resources/html/404-nowiki.htm',
    'config' => [
      [
        'file' => 'InitialiseSettings.php',
        'key' => '*wiki',
      ],
      [
        'file' => 'LocalSettings.php',
        'executable' => true,
      ],
      [
        'file' => 'PrivateSettings.php',
        'key' => '*wiki',
      ],
    ],
  ],

  # Demo
  #'demo-dicoado' => [
  #  'server' => 'demo\\.(?P<lang>fr|de|it|rm-vl|en)\\.dicoado\\.org',
  #  'variables' => [
  #    [
  #      'variable' => 'lang',
  #      'file' => 'versions-demo.php',
  #    ],
  #  ],
  #  'suffix' => 'demowiki',
  #  'wikiID' => '$langdemowiki',
  #  'config' => [
  #    [
  #      'file' => 'InitialiseSettings.php',
  #      'key' => '*demowiki',
  #    ],
  #    [
  #      'file' => 'LocalSettings.php',
  #      'executable' => true,
  #    ],
  #    [
  #      'file' => 'PrivateSettings.php',
  #      'key' => '*demowiki',
  #    ],
  #    [
  #      'file' => 'DemoSettings.php',
  #      'key' => '*wiki',
  #    ],
  #  ],
  #],

];
